import matplotlib.pyplot as plt

from portfolio import *  # Internal project script, portfolio cls
from config import Configuration


if __name__ == "__main__":

    c = Configuration()  # configuration class with task parameters
    portfolio = Portfolio(c)  # the object of Portfolio class
    portfolio.backtest()

    # Plot portfolio value given in ETH amount
    # As soon as ETH is a basic unit of simple strategy example it was chosen to evaluate portfolio
    plt.plot(portfolio.value_history)
    plt.title(f"Portfolio history {portfolio.c.token_name}/ETH")
    plt.xlabel("t")
    plt.ylabel("ETH")
    plt.savefig(c.save_dir / "portfolio_value_evolution.png")

    init_str = f"Initial portfolio value evaluated in ETH: {portfolio.value_history[0]: .3f}"
    final_str = f"Final portfolio value evaluated in ETH: {portfolio.value_history[-1]: .3f}"
    print(init_str)
    print(final_str)

    with open(c.save_dir / "log.txt", "w") as f:
        f.write(init_str + "\n")
        f.write(final_str + "\n")
    f.close()

    with open(c.save_dir / "price_and_value_history.csv", "w") as f:
        f.write("price,value\n")
        for i in range(c.planning_horizon):
            f.write(str(portfolio.price[i]) + "," + str(portfolio.value_history[i]) + "\n")
    f.close()
