# Mellow task 

This project is developed to solve a test problem for a job application.

## Project structure

Note that user changes only config.py script to initialize 
parameters.

![The project structure](mellot_task_dependencies.png)

## Installation

The following command clone project to a local directory:
```
git clone https://gitlab.com/zhilenkov/mellow_task.git
```

Run the command below to install needed packages:
```
pip3 install -r requirements.txt
```

## Execution

Define initial values and strategy in ``config.py`` file. 
The ``main.py`` script executes the project.
```
python3 main.py
```
The results of last run will be placed to the directory
'log', which will be created if it doesn't exist.