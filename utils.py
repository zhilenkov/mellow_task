import numpy as np
import matplotlib.pyplot as plt

from config import Configuration


class Utils:
    def __init__(self, c: Configuration):
        """
        :param c: Configuration of task

        -------------------------------
        The Utils class contains methods to simulate the price history if the initial price value is given

        Methods:

            simulate_price
            plot_price

        """
        self.c = c

    def simulate_price(self):
        """
            - Get the initial value of token/ETH pair from configuration and apply Brownian motion to evolve the price
            - All price changes are given in a percentage value
        :return:
            - None
        """
        price = self.c.token_eth
        print(f"Initial price {self.c.token_name}/ETH: {price}")
        percent_changes = np.random.normal(self.c.mu, self.c.sigma, size=(self.c.planning_horizon,))
        price_history = price * np.cumprod(1 + percent_changes)
        return price_history

    def plot_price(self, prices=None):
        """
        - This function plot mock price history and save results to log_experiments directory

        :param prices:
            - The array-like object with token/ETH price history
            - If the price is None then we call 'simulate_price' to create price
        :return:
            - None
        """
        if prices is None:
            prices = self.simulate_price()

        plt.plot(prices)
        plt.xlabel("t")
        plt.ylabel(f"{self.c.token_name}/ETH")
        plt.title(f"Price history {self.c.token_name}/ETH")
        plt.savefig(self.c.save_dir / "price_history.png")
        plt.clf()

        return
