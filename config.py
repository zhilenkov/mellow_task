from collections import namedtuple
from pathlib import Path

State = namedtuple("State", ("x", "y"))


class Configuration:
    def __init__(self):
        """
        - This class contains user-defined parameters of the task and auxiliary values
        """

        self.save_dir = Path("log")
        if not self.save_dir.exists():
            self.save_dir.mkdir(parents=True)

        self.token_name = "USDC"

        self.planning_horizon = 500

        self.mu = 0.16 / self.planning_horizon
        self.sigma = 0.02

        self.eth_price = 4118.1  # Eth price in USD
        self.token_price = 1.0002  # token (USDC) price in USD

        self.token_eth = self.token_price / self.eth_price
        self.eth_token = self.eth_price / self.token_price

        # Initial amount of tokens
        self.init_token_amount = int(1e6)

    @staticmethod
    def strategy(price: float, state: State) -> tuple:
        """
        - User defined strategy should be placed to the body of this function
        ----------
            :param price: a float number denoting current token price comparing to ETH
            :param state: a State object given as named tuple with 'x' and 'y' float components
            (example: call state.x to get 'x' value)
            :return: (x, y) tuple
        """
        x = state.x
        y = price * state.x
        return x, y
