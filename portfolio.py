import copy

from config import *
from utils import Utils


class Portfolio:
    def __init__(self, c: Configuration):
        """
        - This class represent the portfolio and its evolution over the planning horizon.
        - It has an option to backtest a given strategy (it should be defined in Configuration class)
        ----------------------

            :param c: The object of Configuration class
        """
        self.c = c
        self.u = Utils(self.c)  # Contains auxiliary functions

        # Define price history and initial state
        self.price = self.u.simulate_price()
        self.u.plot_price(self.price)
        self.x = self.c.init_token_amount
        self.y = self.price[0] * self.x

        self.state = State(self.x, self.y)

        self.history = [copy.deepcopy(self.state)]
        self.init_value = self.state.x * self.price[0] + self.state.y
        self.value_history = [self.init_value]

    def eval_portfolio(self, t):
        """
        - This function evaluates the value of portfolio for given time step.
        - The value of portfolio is calculated in ETH units.
        ----------------------
            :param t: int
            :return: value: float
        """
        value = self.history[t].x * self.price[t] + self.history[t].y
        return value

    def backtest(self):
        """
        - This function evaluates the user-defined strategy from Configuration class and collect portfolio history
        ----------------------
            :return: None
        """
        strategy = self.c.strategy
        for t in range(1, self.c.planning_horizon):
            x, y = strategy(self.price[t], self.state)
            new_state = State(x, y)
            self.history.append(new_state)
            self.value_history.append(self.eval_portfolio(t))
        return
